from django.contrib import admin
from .models import *


class PostAdmin(admin.ModelAdmin):
    list_filter = ('title', 'created','updated')
    list_display = ('title', 'created','updated','status')
    search_fields = ('title', 'content')


class CommentAdmin(admin.ModelAdmin):
    list_display = ('post', 'author', 'text', 'timestamp')
    list_filter = ('post', 'author')
    search_fields = ('text', 'author', 'post')

class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'site', 'phone')
    list_filter = ('user',)
    search_fields = ('user', 'site', 'phone','instagram','skype')


admin.site.register(Book, PostAdmin)
admin.site.register(Comment, CommentAdmin)
admin.site.register(UserProfile,ProfileAdmin)
admin.site.register(Favorite)
admin.site.register(BlogComment)
admin.site.register(Chapter)