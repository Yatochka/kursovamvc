from django.db import models
from django.contrib.auth.models import User
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


class Book(models.Model):
    STATUS_CHOISES = (
        ('Published', 'Published'),
        ('Draft', 'Draft'),
    )

    author = models.ForeignKey(User,on_delete=models.CASCADE ,default=1)
    title = models.CharField(max_length=50, blank=False, null=False)
    description = models.CharField(max_length=500,blank=True, null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    likes = models.ManyToManyField(User, blank=True,related_name='post_likes' )
    status = models.CharField(max_length=10, default='Draft', choices=STATUS_CHOISES)
    image = models.FileField(blank=True, upload_to='post_photos/', default='post_photos/gvoz.jpg' )

    def save(self, *args, **kwargs):

        super(Book, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("books:post-detail", kwargs={"pk": self.pk})


class Chapter(models.Model):
    title = models.CharField(max_length=30, blank=False, null=False)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    book = models.ForeignKey(Book, on_delete=models.CASCADE, default=1)

    def __str__(self):
        return self.title


class Comment(models.Model):
    post = models.ForeignKey(Book, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}-{}'.format(self.post.title, str(self.author.username))

    def get_absolute_url(self):
        return reverse('books:post-detail', kwargs={'pk': self.pk})

    def destruction(self, time):
        if time == self.timestamp:
            self.delete()


class UserProfile(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)
    site = models.CharField(max_length=100, default='None')
    phone = models.CharField(max_length=13,default='None')
    instagram = models.CharField(max_length=30, default='None')
    skype = models.CharField(max_length=30,default='None')
    avatar = models.ImageField(blank=True, upload_to='user_photos/', )

    def save(self, *args, **kwargs):
        super(UserProfile, self).save(*args, **kwargs)

    def __str__(self):
        return self.user.username


class Blog(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=50, blank=False, null=False)
    content = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title


class Favorite(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)

    def __str__(self):
        return '{}-{}'.format(self.book.title, str(self.user.username))

    def get_absolute_url(self):
        return reverse("books:post-detail", kwargs={"pk": self.pk})


class BlogComment(models.Model):
    post = models.ForeignKey(Blog, on_delete=models.CASCADE)
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}-{}'.format(self.post.title, str(self.author.username))

    def get_absolute_url(self):
        return reverse('books:blog-detail', kwargs={'pk': self.pk})

    def destruction(self, time):
        if time == self.timestamp:
            self.delete()

class GlobagChatMessage(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=500)
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '{}-{}'.format( str(self.user.username), self.text)