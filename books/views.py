from django.shortcuts import render, get_object_or_404, redirect
from .models import *
from .forms import *
from django.contrib.auth import login, authenticate
from django.views.generic import RedirectView, View
from django.db.models import Q
from django.contrib.auth.models import User
from django.utils import timezone
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.forms import UserChangeForm, PasswordChangeForm
from django.contrib.auth import update_session_auth_hash


def index(request, variable='-created'):
    template = 'pages/post-list.html'
    posts = Book.objects.filter(status='Published').order_by(variable)

    page = request.GET.get('page', 1)

    paginator = Paginator(posts, 10)
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)
    content = {
        'items':  items,
    }
    return render(request, template, content)


def post_detail(request, pk):
    template = 'pages/post-detail.html'
    post = get_object_or_404(Book, pk=pk)
    chapters = Chapter.objects.filter(book=post)
    fav = 1
    user = request.user
    if user.is_authenticated:
        if not Favorite.objects.filter(user=request.user, book=post):
            fav = 2
        else:
            fav = 3

    comments = Comment.objects.filter(post=post)
    if request.method == "POST":
        form = CommentForm(request.POST or None)
        if form.is_valid():
            text = request.POST.get('text')
            comment = Comment.objects.create(post=post, author=request.user, text=text)
            comment.save()
            return redirect(request.META['HTTP_REFERER'])
    else:
        form = CommentForm()
    content = {
        'post': post,
        'comments': comments,
        'form': form,
        'chapters': chapters,
        'favorite': fav
    }
    return render(request, template, content)


def notes_list(request):
    template = 'pages/post-list.html'
    notes = True
    posts = Book.objects.filter(status='Draft').order_by('-created')
    page = request.GET.get('page', 1)

    paginator = Paginator(posts, 10)
    try:
        items = paginator.page(page)
    except PageNotAnInteger:
        items = paginator.page(1)
    except EmptyPage:
        items = paginator.page(paginator.num_pages)
    content = {
        'items': items,
        'notes': notes
    }
    return render(request, template, content)


def post_new(request):
    template = 'pages/post-edit.html'

    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.created = timezone.now()
            post.save()
        return redirect('books:post-list', '-created')
    else:
        form = PostForm()

    content = {
        'form': form
    }
    return render(request, template, content)


def post_edit(request, pk):
    template = 'pages/post-edit.html'
    post = get_object_or_404(Book, pk=pk)
    if request.method == 'POST':
        form = PostForm(request.POST, request.FILES,instance=post)
        if form.is_valid():
            form.save()
        return redirect('books:post-list', '-created')
    else:
        form = PostForm(instance=post)

    content = {
        'form': form
    }
    return render(request, template, content)


def post_delete(request, pk):
    post = get_object_or_404(Book, pk=pk)
    post.delete()
    return redirect('books:post-list', '-created')


def delete_comment(request, pk):
    comment = Comment.objects.get(pk=pk)
    if comment.author == request.user:
        comment.is_removed = True
        comment.save()
        comment.delete()
    return redirect(request.META['HTTP_REFERER'])

def chaper_detail(request, pk):
    template = 'pages/chapter.html'
    chapter = get_object_or_404(Chapter, pk=pk)
    content = {
        'chapter': chapter,
    }
    return render(request, template, content)


def chapter_new(request, pk):
    template = 'pages/post-edit.html'
    book = get_object_or_404(Book, pk=pk)
    if request.method == 'POST':
        form = ChapterForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.book = book
            post.timestamp = timezone.now()
            post.save()
        return redirect('books:post-detail', pk)
    else:
        form = ChapterForm()

    content = {
        'form': form
    }
    return render(request, template, content)


def chapter_edit(request, pk):
    template = 'pages/post-edit.html'
    post = get_object_or_404(Chapter, pk=pk)
    if request.method == 'POST':
        form = ChapterForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
        return redirect('books:post-detail', pk)
    else:
        form = ChapterForm(instance=post)

    content = {
        'form': form
    }
    return render(request, template, content)


def chapter_delete(request, pk):
    post = get_object_or_404(Chapter, pk=pk)
    post.delete()
    return redirect('books:post-detail', pk)

def search(request):
    template = 'pages/post-list.html'
    query = request.GET.get('q')
    if query:
        result = Book.objects.filter(Q(title__icontains=query) | Q(content__icontains=query)).order_by("-created")
    else:
        result = Book.objects.filter(status='Published').order_by("-created")

    content = {
        'posts': result
    }
    return render(request, template, content)


class PostLikeToggle(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        pk = self.kwargs.get("pk")
        print(pk)
        obj = get_object_or_404(Book,pk=pk)
        url_ = obj.get_absolute_url()
        user = self.request.user
        if user.is_authenticated:
            if user in obj.likes.all():
                obj.likes.remove(user)
            else:
                obj.likes.add(user)
        return url_
'''
    User stuff
'''

def signup(request):
    template = 'registration/registration.html'
    if request.method == 'POST' :
        form = SignUpForm(request.POST)
        profile_form = ProfileForm(request.POST, request.FILES)
        if form.is_valid() and profile_form.is_valid():
            form.save()
            profile = profile_form.save(commit=False)
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            profile.user = user
            profile.birth_date = profile_form.cleaned_data.get('birth_date')
            #profile.avatar = profile_form.cleaned_data.get('file')

            profile.save()
            login(request, user)
            return redirect('books:post-list' , '-created')
    else:
        form = SignUpForm()
        profile_form = ProfileForm()
    return render(request, template, {'form': form, 'profile_form': profile_form })


def profile(request, pk):
    template = 'user/profile.html'

    user = get_object_or_404(User, pk=pk)
    user_profile = UserProfile.objects.filter(user=user)
    favorites = Favorite.objects.filter(user=user)
    content = {
        'user': user,
        'profile': user_profile,
        'favorites': favorites
    }
    return render(request, template, content)


def delete_user(request, pk):
    user = get_object_or_404(User, pk=pk)
    user_profile = get_object_or_404(UserProfile, user=user)
    user_profile.delete()
    user.delete()
    return redirect('books:books-list', '-created')


'''
    blog stuff
'''

def blog_list(request):
    template = 'pages/blog-list.html'
    posts = Blog.objects.all().order_by("-timestamp")
    page = request.GET.get('page', 1)

    paginator = Paginator(posts, 10)
    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        posts = paginator.page(1)
    except EmptyPage:
        posts = paginator.page(paginator.num_pages)
    content = {
        'posts': posts,
    }
    return render(request, template, content)


def blog_new(request):
    template = 'pages/post-edit.html'

    if request.method == 'POST':
        form = BlogForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.timestamp = timezone.now()
            post.save()
        return redirect('books:blog-list')
    else:
        form = BlogForm()

    content = {
        'form': form
    }
    return render(request, template, content)


def blog_edit(request, pk):
    template = 'pages/post-edit.html'
    post = get_object_or_404(Blog, pk=pk)
    if request.method == 'POST':
        form = BlogForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
        return redirect('books:blog-list')
    else:
        form = BlogForm(instance=post)

    content = {
        'form': form
    }
    return render(request, template, content)


def blog_delete(request, pk):
    post = get_object_or_404(Blog, pk=pk)
    post.delete()
    return redirect('books:post-list')


def blog_detail(request, pk):
    template = 'pages/blog-detail.html'
    post = get_object_or_404(Blog, pk=pk)
    comments = BlogComment.objects.filter(post=post)
    if request.method == "POST":
        form = BlogCommentForm(request.POST or None)
        if form.is_valid():
            text = request.POST.get('text')
            comment = BlogComment.objects.create(post=post, author=request.user, text=text)
            comment.save()
            return redirect(request.META['HTTP_REFERER'])
    else:
        form = BlogCommentForm()
    content = {
        'post': post,
        'comments': comments,
        'form': form
    }
    return render(request, template, content)


'''
    Add/Remove favorite
'''

class AddToFavorite(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        pk = self.kwargs.get("pk")
        print(pk)
        obj = get_object_or_404(Book,pk=pk)
        url_ = obj.get_absolute_url()
        user = self.request.user
        if user.is_authenticated:
            Favorite.objects.create(user=user, book=obj)
        return url_

class RemoveFromFavorite(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        pk = self.kwargs.get("pk")
        print(pk)
        obj = get_object_or_404(Book,pk=pk)
        url_ = obj.get_absolute_url()
        user = self.request.user
        fav = get_object_or_404(Favorite, book=obj, user=user)
        if user.is_authenticated:
            fav.delete()
        return url_


'''
    Chat methods
'''
def chat(request):
    template = 'pages/chat.html'

    comments = GlobagChatMessage.objects.all().order_by('-timestamp')
    if request.method == "POST":
        form = GlobalChatMessageForm(request.POST or None)
        if form.is_valid():
            text = request.POST.get('text')
            comment = GlobagChatMessage.objects.create( author=request.user, text=text)
            comment.save()
            return redirect('books:chat')
    else:
        form = CommentForm()
    content = {
        'comments': comments,
        'form': form,
    }
    return render(request, template, content)

def globa_delete(request, pk):
    post = get_object_or_404(GlobagChatMessage, pk=pk)
    post.delete()
    return redirect('books:chat')


'''
    Settings
'''


def user_settings(request):
    if request.method == 'POST':
        form = EditProfileForm(request.POST, instance=request.user)

        if form.is_valid():
            form.save()
            return redirect('books:post-list', '-created')
        else:
            return redirect('user-settings')
    else:
        form = EditProfileForm(instance=request.user)
        args = {'form': form, }
    return render(request, 'user/user-settings.html', args)


def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user = request.user)

        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return redirect('books:post-list', '-created')
        else:
            return redirect('change-password')
    else:
        form = PasswordChangeForm(user=request.user)
        args = {'form': form}
    return render(request, 'user/change-password.html', args)
