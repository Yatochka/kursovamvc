
from django.contrib import admin
from django.urls import path, include
from books.views import *
from django.contrib.auth.decorators import login_required

app_name = 'books'

urlpatterns = [
    path('home/filter=<variable>', index, name='post-list'),
    path('post/<int:pk>/', post_detail, name='post-detail'),
    path('signup/', signup, name='signup'),
    path('search/', search, name='search'),
    path('create-new/', post_new, name='create-post'),
    path('edit/<int:pk>/', post_edit, name='post-edit'),
    path('<int:pk>/like/', PostLikeToggle.as_view(), name="like-toggle"),
    path('delete/post/<int:pk>/', post_delete, name='post-delete'),
    path('edit/post/<int:pk>', post_edit, name='post-edit'),
    path('(<int:pk>/delete-comment/', delete_comment, name='delete-comment'),
    path('profile/<int:pk>/', profile, name='user-profile'),
    path('blogs/', blog_list, name='blog-list'),
    path('create-blog/', blog_new, name='create-blog'),
    path('delete/blog/<int:pk>/', blog_delete, name='blog-delete'),
    path('edit/blog/<int:pk>', blog_edit, name='blog-edit'),
    path('blog/<int:pk>/', blog_detail, name='blog-detail'),
    path('favotire/post/<int:pk>', AddToFavorite.as_view(), name='add-favorite'),
    path('delete-favotire/post/<int:pk>', RemoveFromFavorite.as_view(), name='remove-favorite'),
    path('chat/', chat, name='chat'),
    path('delete-message/<int:pk>/chat', globa_delete, name='global-delete'),
    path('user-settings/', user_settings, name='user-settings'),
    path('change-password/', change_password, name='change-password'),
    path('delete-user/', delete_user, name="delete-user"),
    path('new-chapter/post/<int:pk>/', chapter_new, name='create-chapter'),
    path('edit-chapter/<int:pk>/', chapter_edit, name='chapter-edit'),
    path('delete/chapter/<int:pk>/', chapter_delete, name='chapter-delete'),
    path('chapter/<int:pk>', chaper_detail, name='chapter-detail'),
    path('notes/', notes_list, name='notes'),


]
